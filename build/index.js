const { run } = require('runjs') // 引入 runjs , 可以使用node运行
const chalk = require('chalk') // chalk是一个 可以修改终端输出字符样式的 npm 包，提高在终端输出的信息的辨识度
const config = require('../vue.config.js') // 引入vue-cli 配置文件
// process是node的全局模块，作用比较直观。可以通过它来获取node进程相关的信息，比如运行node程序时的命令行参数。或者设置进程相关信息，比如设置环境变量。
// process.argv 获取命令行参数，返回一个数组。
// 参数0: /Users/a/.nvm/versions/node/v6.1.0/bin/node
// 参数1: /Users/a/Documents/argv.js
// 参数2: --env
// 参数3: production

const rawArgv = process.argv.slice(2)
const args = rawArgv.join(' ')

/*
1.chalk
  chalk是一个 可以修改终端输出字符样式的 npm 包，提高在终端输出的信息的辨识度
2.process
  process是node的全局模块，作用比较直观。可以通过它来获取node进程相关的信息，比如运行node程序时的命令行参数。或者设置进程相关信息，比如设置环境变量。
3.express
  express连接中间件connect,connect维护了一个中间件栈，栈stack：数据结构，每次调用use，都会向这个应用（app)实例的栈（stack）推入一个带路径和处理函数的对象。
 */
if (process.env.npm_config_preview || rawArgv.includes('--preview')) {
  const report = rawArgv.includes('--report')

  run(`vue-cli-service build ${args}`)

  const port = 9526
  const publicPath = config.publicPath // 引入 配置文件的 基本路径
  // express连接中间件connect,connect维护了一个中间件栈，栈stack：数据结构，每次调用use，都会向这个应用（app)实例的栈（stack）推入一个带路径和处理函数的对象。
  var connect = require('connect')
  // 实现静态资源访问，引入静态资源访问模块，serviceStatic 返回值是一个方法，调用返回的这个方法就可以开启静态资源服务
  var serveStatic = require('serve-static')
  const app = connect()

  app.use(
    publicPath,
    serveStatic('./dist', {
      index: ['index.html', '/']
    })
  )
  // 终端打印出来的信息，通过 chalk.green() 修改颜色，提高信息辨识度。
  app.listen(port, function () {
    console.log(chalk.green(`> Preview at  http://localhost:${port}${publicPath}`))
    if (report) {
      console.log(chalk.green(`> Report at  http://localhost:${port}${publicPath}report.html`))
    }
  })
} else {
  run(`vue-cli-service build ${args}`)
}
/* 
  https://juejin.cn/post/6941284747918671879#heading-7
 */
