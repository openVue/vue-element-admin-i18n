'use strict'
//  引入path模块 拼接文件路径
const path = require('path')
//  引入settings配置文件
const defaultSettings = require('./src/settings.js')
const isProd = process.env.NODE_ENV === 'production'
const name = defaultSettings.title || 'vue Element Admin' // 标题
const port = process.env.port || process.env.npm_config_port || 8080 // 开发端口
const compressionWebpackPlugin = require('compression-webpack-plugin')
const uglifyJsPlugin = require('uglifyjs-webpack-plugin')
//  封装拼接方法
function resolve(dir) {
  return path.join(__dirname, dir)
}
// 生产环境和开发环境要区别开,获取插件plugin配置
function getPlugins() {
  let plugins = []
  let productionGzipExtensions = ['html', 'js', 'css']
  // 生产环境配置
  if (isProd) {
    // 代码压缩
    plugins.push(
      new uglifyJsPlugin({
        uglifyOptions: {
          output: {
            comments: false // 去掉注释
          },
          warnings: false,
          // 生产环境自动删除console
          compress: {
            drop_debugger: true,
            drop_console: true,
            pure_funcs: ['console.log']
          }
        },
        sourceMap: false,
        parallel: true
      }),
      // gzip压缩
      new compressionWebpackPlugin({
        filename: '[path].gz[query]',
        algorithm: 'gzip',
        test: new RegExp('\\.(' + productionGzipExtensions.join('|') + ')$'),
        threshold: 10240, // 只有大小大于该值的资源会被处理 10240
        minRatio: 0.8, // 只有压缩率小于这个值的资源才会被处理
        deleteOriginalAssets: false // 删除原文件
      })
    )
  }
  return plugins
}

module.exports = {
  /**
   * 如果计划在子路径下部署站点，则需要设置publicPath，
   * 例如GitHub页面。如果您计划将站点部署到https://foo.github.io/bar/
   * 那么publicPath应该设置为"/bar/"。
   * 在大多数情况下请使用'/' !!
   * 细节:https://cli.vuejs.org/config/ publicpath
   */
  publicPath: '/',
  outputDir: 'dist',
  assetsDir: 'static',
  lintOnSave: process.env.NODE_ENV === 'development',
  productionSourceMap: false,
  // 如果你的前端应用和后端API服务器没有运行在同一个主机上，你需要在开发环境下将API请求代理到API服务器。
  // 这个问题可以通过vue.config.js中的devServer.proxy来配置。
  devServer: {
    port: port,
    open: true,
    overlay: {
      warnings: false,
      errors: true
    },
    before: require('./mock/mock-server.js')
  },
  // webpack相关配置
  configureWebpack: {
    // 在webpack的名称字段中提供应用程序的标题，这样
    // 它可以通过index.html来注入正确的标题。
    name,
    plugins: getPlugins(),
    mode: isProd ? 'production' : 'development', // 方便开发调试
    devtool: isProd ? false : 'source-map',
    //关闭 webpack 的性能提示
    performance: {
      hints: false
    },
    resolve: {
      alias: {
        '@': resolve('src')
      }
    }
  },
  // chainWebpack是一个函数，会接收一个基于webpack-china的chainableConfig 实例，允许对内部的webpack配置进行更细粒度的修改。
  chainWebpack(config) {
    // 可以提高第一屏的速度，建议开启预加载
    config.plugin('preload').tap(() => [
      {
        rel: 'preload',
        // to ignore runtime.js
        // https://github.com/vuejs/vue-cli/blob/dev/packages/@vue/cli-service/lib/config/app.js#L171
        fileBlacklist: [/\.map$/, /hot-update\.js$/, /runtime\..*\.js$/],
        include: 'initial'
      }
    ])

    // 当页面太多时，会导致太多无意义的请求
    config.plugins.delete('prefetch')

    // set svg-sprite-loader
    config.module.rule('svg').exclude.add(resolve('src/icons')).end()
    config.module
      .rule('icons')
      .test(/\.svg$/)
      .include.add(resolve('src/icons'))
      .end()
      .use('svg-sprite-loader')
      .loader('svg-sprite-loader')
      .options({
        symbolId: 'icon-[name]'
      })
      .end()

    config.when(process.env.NODE_ENV !== 'development', (config) => {
      config
        .plugin('ScriptExtHtmlWebpackPlugin')
        .after('html')
        .use('script-ext-html-webpack-plugin', [
          {
            // `runtime` must same as runtimeChunk name. default is `runtime`
            inline: /runtime\..*\.js$/
          }
        ])
        .end()
      config.optimization.splitChunks({
        chunks: 'all',
        cacheGroups: {
          libs: {
            name: 'chunk-libs',
            test: /[\\/]node_modules[\\/]/,
            priority: 10,
            chunks: 'initial' // only package third parties that are initially dependent
          },
          elementUI: {
            name: 'chunk-elementUI', // split elementUI into a single package
            priority: 20, // the weight needs to be larger than libs and app or it will be packaged into libs or app
            // 重量需要大于libs和app，否则会被打包成libs或app
            test: /[\\/]node_modules[\\/]_?element-ui(.*)/ // in order to adapt to cnpm
          },
          commons: {
            name: 'chunk-commons',
            test: resolve('src/components'), // can customize your rules 可以自定义规则
            minChunks: 3, //  minimum common number 最低常见的数量
            priority: 5,
            reuseExistingChunk: true
          }
        }
      })
      // https:// webpack.js.org/configuration/optimization/#optimizationruntimechunk
      config.optimization.runtimeChunk('single')
    })
  }
}
